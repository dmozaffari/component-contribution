"""Command line script for managing groups in equilibrator-cache."""
# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import logging
from os.path import join

import click
import pandas as pd
import quilt
from equilibrator_cache import Compound, create_compound_cache_from_quilt
from equilibrator_cache.api import DEFAULT_QUILT_PKG as CACHE_QUILT_PKG
from equilibrator_cache.api import DEFAULT_QUILT_VERSION as CACHE_QUILT_VERSION
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from tqdm import tqdm

import click_log
from component_contribution import (
    DEFAULT_QUILT_PKG,
    DEFAULT_QUILT_VERSION,
    ComponentContributionTrainer,
)
from component_contribution.training_data import FullTrainingData
from support.group_decompose import GroupDecomposer, GroupDecompositionError
from support.groups import DEFAULT_GROUPS_DATA
from support.molecule import Molecule, OpenBabelError


logger = logging.getLogger()
click_log.basic_config(logger)
Session = sessionmaker()

DEFAULT_DATABASE_URL = "sqlite:///compounds.sqlite"


@click.group()
@click.help_option("--help", "-h")
@click_log.simple_verbosity_option(
    logger,
    default="INFO",
    show_default=True,
    type=click.Choice(["CRITICAL", "ERROR", "WARN", "INFO", "DEBUG"]),
)
def cli():
    """Command line interface to populate and update the equilibrator cache."""
    pass


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "--db-url",
    metavar="URL",
    default=DEFAULT_DATABASE_URL,
    show_default=True,
    help="A string interpreted as an rfc1738 compatible database URL.",
)
@click.option(
    "--quilt_pkg",
    metavar="PATH",
    default=CACHE_QUILT_PKG,
    show_default=True,
    help="Quilt package name for the compound cache.",
)
@click.option(
    "--quilt_version",
    metavar="VERSION",
    default=CACHE_QUILT_VERSION,
    show_default=True,
    help="Quilt package version for the compound cache.",
)
@click.option(
    "--batch-size",
    type=int,
    default=30000,
    show_default=True,
    help="The size of batches of compounds considered at a time.",
)
def decompose(
    db_url: str, quilt_pkg: str, quilt_version: str, batch_size: int
) -> None:
    """Calculate and store thermodynamic information for compounds."""
    engine = create_engine(db_url)
    session = Session(bind=engine)

    # Query for all compounds.
    query = session.query(
        Compound.id, Compound.inchi_key, Compound.smiles
    ).filter(Compound.smiles.isnot(None), Compound.group_vector.is_(None))

    logger.debug("decomposing all compounds with structures")
    input_df = pd.read_sql_query(query.statement, query.session.bind)

    group_decomposer = GroupDecomposer()

    with tqdm(total=len(input_df), desc="Analyzed") as pbar:
        for index in range(0, len(input_df), batch_size):
            view = input_df.iloc[index : index + batch_size, :]
            compounds = []
            for row in view.itertuples(index=False):
                try:
                    # it is important to use here the SMILES representation
                    # of the molecule, and not the InChI, since it hold the
                    # information about the most abundant species at pH 7.
                    mol = Molecule.FromSmiles(row.smiles)
                    decomposition = group_decomposer.Decompose(
                        mol, ignore_protonations=False, raise_exception=True
                    )
                    group_vector = decomposition.AsVector()
                    compounds.append(
                        {"id": row.id, "group_vector": list(group_vector.flat)}
                    )
                    logger.debug(
                        "Decomposition of Compound(id=%d, inchi_key=%s): %r",
                        row.inchi_key,
                        row.id,
                        decomposition,
                    )
                except OpenBabelError as e:
                    compounds.append({"id": row.id, "group_vector": list()})
                    logger.debug(str(e))
                except GroupDecompositionError as e:
                    compounds.append({"id": row.id, "group_vector": list()})
                    logger.debug(str(e))

            session.bulk_update_mappings(Compound, compounds)
            session.commit()
            pbar.update(len(view))

    logger.debug("pushing DB changes to quilt (i.e. adding group vectors)")
    quilt.install(quilt_pkg, version=quilt_version, force=True)

    # rebuild the 'compounds' table, now with the new group vectors
    # compounds_df = pd.read_sql_table("compounds", engine)
    # quilt.build(join(quilt_pkg, "compounds", "compounds"), compounds_df)

    # build the group names and add them to quilt as well, this list is
    # needed later by the training scripts, because they cannot access the
    # GroupsData class directly (to get rid of their OpenBabel dependence)
    quilt.build(
        join(quilt_pkg, "groups", "definitions"),
        DEFAULT_GROUPS_DATA.ToDataFrame(),
    )

    quilt.push(quilt_pkg, is_public=True)


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "--quilt_pkg",
    metavar="PATH",
    default=DEFAULT_QUILT_PKG,
    show_default=True,
    help="Quilt package name for the component contribution model.",
)
@click.option(
    "--quilt_version",
    metavar="VERSION",
    default=DEFAULT_QUILT_VERSION,
    show_default=True,
    help="Quilt package version for the component contribution model.",
)
def train(quilt_pkg: str, quilt_version: str) -> None:
    """Train the Component Contribution model and push to quilt."""
    ccache = create_compound_cache_from_quilt()
    train_data = FullTrainingData(ccache)
    cc = ComponentContributionTrainer.train(train_data, mse_inf=1e10)
    quilt.install(quilt_pkg, version=quilt_version, force=True)

    pkg = quilt.load(quilt_pkg)
    pkg._add_group("parameters")
    for param_name, param_value in cc.params._asdict().items():
        pkg.parameters._set([param_name], param_value)
    quilt.build(quilt_pkg, pkg)

    quilt.push(quilt_pkg, is_public=True)


if __name__ == "__main__":
    cli()
